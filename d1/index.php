<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Repetition Control Structure and Array Manipulation</title>
</head>
<body>
	<h1>Repetition Control Structures</h1>
	<h2>While Loop</h2>
	<p><?php whileLoop(); ?></p>

	<h2>Do While Loop</h2>
	<p><?php doWhileLoop(); ?></p>

	<h2>For Loop</h2>
	<p><?php forLoop(); ?></p>

	<h1>Array Manipulation</h1>
	<h2>Types of Arrays</h2>
	<h3>Simple Array</h3>
	<ul>
		<?php foreach ($computerBrands as $brand){ ?>
			<li><?php echo $brand ?></li>
		<?php } ?>
	</ul>

	<h3>Associative Array</h3>
	<ul>
		<?php foreach ($gradePeriods as $period => $grade) { ?>
			<li>Grade in <?= $period ?> is <?= $grade ?></li>
		<?php } ?>
	</ul>

	<h3>Multi-Dimensional Array</h3>
	<ul>
		<?php foreach ($heroes as $team){
			foreach ($team as $member){ ?>
			<li><?php echo $member ?></li>
			<?php }
		} ?>
	</ul>

	<h2>Array Functions</h2>
	<h3>Sorting</h3>
	<pre><?php print_r($sortedBrands) ?></pre>
	<pre><?php print_r($reverseSortedBrands) ?></pre>

	<h3>Append</h3>
	<?php array_push($computerBrands, 'Apple') ?>
	<pre><?php print_r($computerBrands) ?></pre>

	<?php array_unshift($computerBrands, 'Dell') ?>
	<pre><?php print_r($computerBrands) ?></pre>

	<h3>Remove</h3>
	<?php array_pop($computerBrands) ?>
	<pre><?php print_r($computerBrands) ?></pre>

	<?php array_shift($computerBrands) ?>
	<pre><?php print_r($computerBrands) ?></pre>

	<h3>Other Array Functions</h3>
	<h4>In Array</h4>
	<pre><?php echo searchBrands($computerBrands, 'HP') ?></pre>

	<h4>Count</h4>
	<pre><?php echo count($computerBrands) ?></pre>
</body>
</html>
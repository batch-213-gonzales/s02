<?php

//Repetition Control Structures
//While loop: takes a single condition. If the condition evaluates to true, the code inside the block will run.

function whileLoop(){
	$count = 5;

	while($count !== 0){
		echo $count . '<br/>';
		$count--;
	}
}

//Do While loop: it guarantees that the code will be executed at least once.

function doWhileLoop(){
	$count = 20;

	do{
		echo $count . '<br/>';
		$count--;
	} while($count > 0);
}

//For loop: more flexible than while and do-while loops.

function forLoop(){
	for($count = 0; $count <= 20; $count++){
		echo $count . '<br/>';
	}
}

//Array Manipulation
//PHP Arrays are declared using square brackets '[]' or array() function.

$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'); //before PHP 5.4

$newStudentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; //PHP 5.4

//Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba'];
$task = [
	'drink html',
	'eat javascipt',
	'inhale css',
	'bake sass'
];

//Associative Arrays
$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 90.1, 'thirdGrading' => 94.3, 'fourthGrading' => 89.2];

//Two-Dimensional Array
$heroes = [
	['iron man', 'thor', 'hulk'],
	['wolverine', 'cyclops', 'jean grey'],
	['batman', 'superman', 'wonder woman']
];

//Two-Dimensional Associative Array
$ironManPowers = [
	'regular' => ['repulsor blast', 'rocket punch'],
	'signature' => ['unibeam']
];

//Array Functions
//Array Sorting
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);

//Other Array Functions
function searchBrands($brands, $brand){
	return (in_array($brand, $brands)) ? "$brand is in the array." : "$brand is not in the array";
}